VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "View"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Model_ As Model

Property Let Model(Model As Model)
    Set Model_ = Model
End Property

Sub Update()
    Select Case Model_.ind
    Case 0
        Range("A1").Interior.color = RGB(255, 0, 0)
    Case 1
        Range("A1").Interior.color = RGB(0, 255, 0)
    Case Else
        Range("A1").Interior.color = RGB(0, 0, 255)
    End Select
End Sub
